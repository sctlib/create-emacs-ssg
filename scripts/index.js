#!/usr/bin/env node
// import packageJson from "../package.json" assert { type: "json" };
// console.log('packageJson', packageJson)

// What a user should have in their project's `package.json`
const examplePkgJson = {
	scripts: {
		dev: "emacs-ssg watch",
		build: "emacs-ssg build",
		"publish-pages": "emacs-ssg publish-pages",
	},
	"@sctlib/emacs-ssg": {},
};

// with the formatings, spaces and new lines matter
const formatCmd = (commandString) => {
	return `=${commandString}=`;
};
const formatJSON = (obj) => {
	return JSON.stringify(obj, null, 2);
};

// How to proceed, to start using
const instructions = `
# Create a new =emacs-ssg= project (folder & files)
→ dependencies: "emacs" and "inotifywait", the dependencies
→ create a new folder 'my-site', and navigate into it
→ (optional) make the project's folder a git repository (easier to publish)
${formatCmd("git init")}
→ Install @sclib/emacs-ssg
${formatCmd("npm install --save-dev @sctlib/emacs-ssg")}
→ inside the 'package.json.scripts' add 3 scripts and the key ${formatCmd('"@sctlib/emacs-ssg": {}')} with and empty config object
${formatJSON(examplePkgJson)}
→ Run the local development server
${formatCmd("npm run dev")}
It will watch the project folder for changes, and rebuild
→ Create the content/assets folders and files
./content/index.org (text content, org-mode optional)
./assets/css/index.css (css content)
./assets/js/index.js (javascript content)
→ To build a website for publishing, into ./public (no minification)
${formatCmd("mpm run build")}
→ For CI/CD, "to update emacs packages" and build, use
${formatCmd("npm run publish-pages")}
# more info on https://gitlab.com/sctlib/emacs-ssg/
# explore the "node_modules/@sctlib/emacs-ssg" folder for templates/docs
# For a gitlab page deploy use the command (don't forget the =.= as last character)
# ${formatCmd("cp node_modules/@sctlib/emacs-ssg/.gitlab-ci.yml .")}
`;

const main = () => {
	console.info(instructions);
};

main();
